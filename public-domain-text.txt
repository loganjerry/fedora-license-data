# This file is for collecting the actual text found in Fedora
# packages that have used the Callaway short name, "Public Domain" 
# or the SPDX id, "LicenseRef-Fedora-Public-Domain"
# As per the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_callaway_short_name_categories
# 
# This file is for collecting the text of such public domain dedications.
#
# Include the following information:
# 
# Fedora package name
#
# Location of where you found the public domain dedication text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text of the public dedication found that corresponds to the use of 
# the "Public Domain" (previously) or "LicenseRef-Fedora-Public-Domain" SPDX id.
# Remove blank lines.
#
# Copy template below and add yours to top of list, adding a space between entries.

package = 
location = 
text = '''
text here
'''

package = ecl
location = https://gitlab.com/embeddable-common-lisp/ecl/-/blob/develop/src/lsp/cmuutil.lsp
text = '''
This code was written as part of the CMU Common Lisp project at
Carnegie Mellon University, and has been placed in the public domain.
'''

package = gap-pkg-profiling
location = https://github.com/gap-packages/profiling/blob/master/src/md5.cc
text = '''
This code implements the MD5 message-digest algorithm.
The algorithm is due to Ron Rivest.  This code was
written by Colin Plumb in 1993, no copyright is claimed.
This code is in the public domain; do with it what you wish.
Equivalent code is available from RSA Data Security, Inc.
This code has been tested against that, and is equivalent,
except that you don't need to include two pages of legalese
with every copy.
'''

package = icu4j
location = https://github.com/unicode-org/icu/blob/main/icu4j/main/shared/licenses/LICENSE
text = '''
  ICU uses the public domain data and code derived from Time Zone
Database for its time zone support. The ownership of the TZ database
is explained in BCP 175: Procedure for Maintaining the Time Zone
Database section 7.
7.  Database Ownership
    The TZ database itself is not an IETF Contribution or an IETF
    document.  Rather it is a pre-existing and regularly updated work
    that is in the public domain, and is intended to remain in the
    public domain.  Therefore, BCPs 78 [RFC5378] and 79 [RFC3979] do
    not apply to the TZ Database or contributions that individuals make
    to it.  Should any claims be made and substantiated against the TZ
    Database, the organization that is providing the IANA
    Considerations defined in this RFC, under the memorandum of
    understanding with the IETF, currently ICANN, may act in accordance
    with all competent court orders.  No ownership claims will be made
    by ICANN or the IETF Trust on the database or the code.  Any person
    making a contribution to the database or code waives all rights to
    future claims in that contribution or in the TZ Database.
'''

package = pl
location = https://github.com/SWI-Prolog/packages-http/blob/master/examples/calc.pl
text = '''
Copyright (C): Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-semweb/blob/master/murmur.c
text = '''
License: Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-stomp/blob/master/examples/ping.pl
text = '''
@license This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/packages-xpce/blob/master/src/gnu/getdate-source.y
text = '''
This code is in the public domain and has no copyright.
'''

package = pl
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/scripts/swipl-bt
text = '''
This code is in the public domain
'''

package = pl
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/src/pl-hash.c
text = '''
License: Public domain
'''

package = pl
location = https://github.com/SWI-Prolog/swipl-devel/blob/master/src/tools/functions.pm
text = '''
This code is in the public domain
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/md5.lisp
text = '''
This file implements The MD5 Message-Digest Algorithm, as defined in
RFC 1321 by R. Rivest, published April 1992.
It was written by Pierre R. Mai, with copious input from the
cmucl-help mailing-list hosted at cons.org, in November 2001 and
has been placed into the public domain.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/metering.lisp
text = '''
This code is in the public domain and is distributed without warranty
of any kind.
'''

package = pvs-sbcl
location = https://github.com/SRI-CSL/PVS/blob/master/src/Field/decimals.lisp
text = '''
License: Public domain
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
'''

package = python-pdfminer
location = https://github.com/pdfminer/pdfminer.six/blob/ebf7bcdb983f36d0ff5b40e4f23b52525cb28f18/pdfminer/ascii85.py#L3
text = '''
This code is in the public domain.
'''

package = imagej
location = https://imagej.nih.gov/ij/disclaimer.html
text = '''
ImageJ was developed at the National Institutes of Health by an employee of the Federal Government in the course of his official duties. Pursuant to Title 17, Section 105 of the United States Code, this software is not subject to copyright protection and is in the public domain. ImageJ is an experimental system. NIH assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic.
'''

package = gdouros-aegean-fonts
        gdouros-aegyptus-fonts
        gdouros-akkadian-fonts
        gdouros-alexander-fonts
        gdouros-anaktoria-fonts
        gdouros-analecta-fonts
        gdouros-aroania-fonts
        gdouros-asea-fonts
        gdouros-avdira-fonts
        gdouros-musica-fonts
        gdouros-symbola-fonts
location = https://web.archive.org/web/20150625020428/http://users.teilar.gr/~g1951d/
text = '''
in lieu of a licence
Fonts and documents in this site are not pieces of property or merchandise items; they carry no trademark, copyright, license or other market tags; they are free for any use.
'''


package = perl-IO-HTML
location = https://metacpan.org/release/CJM/IO-HTML-1.004/source/examples/detect-encoding.pl
text = '''
This example is hereby placed in the public domain.
You may copy from it freely.

Detect the encoding of files given on the command line
'''

package = dotnet7.0
location = https://github.com/dotnet/fsharp/blob/186d3f6ce267c27f8acaa2c8f3df1472bf2a88bf/src/FSharp.Core/resumable.fs#L2-L3
text = '''
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this software to the public domain worldwide. This software is distributed without any warranty.
'''

package = dotnet7.0
location = https://github.com/dotnet/runtime/blob/e721ae953c61297aec73e8051696f2b9b30148c7/src/coreclr/inc/utilcode.h#L2417-L2425
text = '''
You can use this free for any purpose.  It's in the public domain.  It has no warranty.
'''

package = x11vnc
location = https://github.com/LibVNC/x11vnc/tree/af63109a17f1b1ec8b1e332d215501f11c4a33a0/misc/turbovnc
text = '''
This work has been (or is hereby) released into the public domain by
its author, Karl J. Runge <runge@karlrunge.com>. This applies worldwide.
In case this is not legally possible: Karl J. Runge grants anyone the
right to use this work for any purpose, without any conditions, unless
such conditions are required by law.
'''

package = xmlpull
location = https://github.com/xmlpull-xpp3/xmlpull-xpp3/blob/master/xmlpull/LICENSE.txt
text = '''
All of the XMLPULL API source code, compiled code, and documentation 
contained in this distribution *except* for tests (see separate LICENSE_TESTS.txt)
are in the Public Domain.
XMLPULL API comes with NO WARRANTY or guarantee of fitness for any purpose.
'''

package = xz.java
location = https://git.tukaani.org/?p=xz-java.git;a=blob;f=COPYING;h=8dd17645c4610c3d5eed9bcdd2699ecfac00406b;hb=HEAD
text = ''' 
Licensing of XZ for Java
========================
All the files in this package have been written by Lasse Collin,
Igor Pavlov, and/or Brett Okken. All these files have been put into
the public domain. You can do whatever you want with these files.
This software is provided "as is", without any warranty.
'''

package = libselinux
location = https://github.com/SELinuxProject/selinux/blob/master/libselinux/LICENSE
text = '''
This library (libselinux) is public domain software, i.e. not copyrighted.

Warranty Exclusion
------------------
You agree that this software is a
non-commercially developed program that may contain "bugs" (as that
term is used in the industry) and that it may not function as intended.
The software is licensed "as is". NSA makes no, and hereby expressly
disclaims all, warranties, express, implied, statutory, or otherwise
with respect to the software, including noninfringement and the implied
warranties of merchantability and fitness for a particular purpose.

Limitation of Liability
-----------------------
In no event will NSA be liable for any damages, including loss of data,
lost profits, cost of cover, or other special, incidental,
consequential, direct or indirect damages arising from the software or
the use thereof, however caused and on any theory of liability. This
limitation will apply even if NSA has been advised of the possibility
of such damage. You acknowledge that this is a reasonable allocation of
risk.
'''
